﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CollectBackend.RequestModels;
using CollectBackend.ResponseModels;

namespace CollectBackend.Services
{
    public interface IEntryService
    {
        ICollection<EntryResponseModel> GetAllEntries();
        Task<EntryResponseModel> GetEntryById(Guid id);
        Task<Guid?> AddNewEntry(EntryCreateRequestModel entry);
        Task<Guid?> AddNewCustomEntry(EntryCustomCreateRequestModel entry);
        Task<bool> RemoveEntry(Guid id);
        Task<bool> UpdateEntry(EntryUpdateRequestModel entry);
        Task<ICollection<EntryResponseModel>> GetEntryByCollectionId(Guid id);
    }
}