﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CollectBackend.RequestModels;
using CollectBackend.ResponseModels;

namespace CollectBackend.Services
{
    public interface ICollectionService
    {
        Task<CollectionResponseModel> GetCollectionById(Guid id);
        ICollection<CollectionResponseModel> GetAllCollections();
        Task<Guid?> AddNewCollection(CollectionCreateRequestModel collectionCreate);
        Task<bool> UpdateCollection(CollectionUpdateRequestModel collection);
        Task<bool> RemoveCollection(Guid id);
    }
}