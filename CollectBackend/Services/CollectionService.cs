﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using CollectBackend.Data;
using CollectBackend.Data.Models;
using CollectBackend.RequestModels;
using CollectBackend.ResponseModels;
using Microsoft.EntityFrameworkCore;
using Attribute = CollectBackend.Data.Models.Attribute;

namespace CollectBackend.Services
{
    public class CollectionService : ICollectionService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IEntryService _entryService;
        public CollectionService(ApplicationDbContext applicationDbContext, IEntryService entryService)
        {
            _applicationDbContext = applicationDbContext;
            _entryService = entryService;
        }

        public async Task<CollectionResponseModel> GetCollectionById(Guid id)
        {
            var collection = await _applicationDbContext.Collections.FindAsync(id);
            if (collection == null) return null;
            var returnValue = MapCollectionToCollectionResponseModel(collection);
            return returnValue;
        }

        public ICollection<CollectionResponseModel> GetAllCollections()
        {
            var collections = _applicationDbContext.Collections;
            if (collections.IsNullOrEmpty()) return null;
            var returnValue = collections.Select(collection => MapCollectionToCollectionResponseModel(collection)).ToList();
            return returnValue;
        }

        public async Task<Guid?> AddNewCollection(CollectionCreateRequestModel collectionCreate)
        {
            var toInsertCollection = new Collection()
            {
                Name = collectionCreate.Name,
                Description = collectionCreate.Description
            };

            var toInsertAttributes = collectionCreate.Attributes.Select(attribute => new Attribute() {Name = attribute, Collection = toInsertCollection}).ToList();
            toInsertCollection.Attributes = toInsertAttributes;

            await _applicationDbContext.Collections.AddAsync(toInsertCollection);
            try
            {
                await _applicationDbContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine(e);
                return null;
            }
            return toInsertCollection.CollectionId;
        }

        public async Task<bool> UpdateCollection(CollectionUpdateRequestModel collection)
        {
            var collectionFromDb = await _applicationDbContext.Collections.FindAsync(collection.CollectionId);
            if (collectionFromDb == null) return false;

            collectionFromDb.Name = collection.Name;
            collectionFromDb.Description = collection.Description;
            var attributesToRemove = collectionFromDb.Attributes.Where(x => !collection.Attributes.Contains(x.Name));
            var entryAttributesToRemove = attributesToRemove.SelectMany(x => x.EntryAttributes);
            var attributesToAdd = collection.Attributes.Where(x => !collectionFromDb.Attributes.Any(y => y.Name == x))
                .Select(x => new Attribute(){Name = x,Collection = collectionFromDb});

            _applicationDbContext.EntryAttributes.RemoveRange(entryAttributesToRemove);
            _applicationDbContext.Attributes.RemoveRange(attributesToRemove);
            await _applicationDbContext.Attributes.AddRangeAsync(attributesToAdd);
            _applicationDbContext.Collections.Update(collectionFromDb);

            await _applicationDbContext.SaveChangesAsync();

            return true;
        }

        // public  async Task<bool> UpdateCollection(CollectionUpdateRequestModel collection)
        // {
        //     var currentCollection = await _applicationDbContext.Collections.FindAsync(collection.CollectionId);
        //     if (currentCollection == null) return false;
        //
        //     currentCollection.Description = collection.Description;
        //     currentCollection.Name = collection.Name;
        //
        //     var attributesToAdd = collection.Attributes.Where(x =>  currentCollection.Attributes.All(y => y.Name != x))
        //         .Select(x => new Attribute() {Collection = currentCollection, Name = x}).ToArray();
        //     await _applicationDbContext.Attributes.AddRangeAsync(attributesToAdd);
        //     _applicationDbContext.Collections.Update(currentCollection);
        //
        //     
        //     var attributesToRemove = currentCollection.Attributes.Where(x => !collection.Attributes.Contains(x.Name));
        //     var entryAttributesToRemove =
        //         _applicationDbContext.EntryAttributes.Where(x => !collection.Attributes.Contains(x.Attribute.Name));
        //     _applicationDbContext.EntryAttributes.RemoveRange(entryAttributesToRemove);
        //     _applicationDbContext.Attributes.RemoveRange(attributesToRemove);
        //
        //     try
        //     {
        //         await _applicationDbContext.SaveChangesAsync();
        //     }
        //     catch (DbUpdateException e)
        //     {
        //         Console.WriteLine(e);
        //         return false;
        //     }
        //
        //     return true;
        // }

        public async Task<bool> RemoveCollection(Guid id)
        {
            var toRemove = await _applicationDbContext.Collections.FindAsync(id);
            if (toRemove == null) return false;


            _applicationDbContext.Attributes.RemoveRange(toRemove.Attributes);
            await _applicationDbContext.SaveChangesAsync();
            foreach (var entryAttribute in toRemove.Entries.ToArray())
            {
                await _entryService.RemoveEntry(entryAttribute.EntryId);
            }
            await _applicationDbContext.SaveChangesAsync();
            _applicationDbContext.Collections.Remove(toRemove);

            try
            {
                await _applicationDbContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

        private static CollectionResponseModel MapCollectionToCollectionResponseModel(Collection collection)
        {
            var collectionReturn = new CollectionResponseModel
            {
                CollectionId = collection.CollectionId, Description = collection.Description,
                Name = collection.Name,
                Attributes = collection.Attributes.Select(x => x.Name).ToArray()
        };
            return collectionReturn;
        }
    }
}