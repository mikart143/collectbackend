﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Castle.Core.Internal;
using CollectBackend.Data;
using CollectBackend.Data.Models;
using CollectBackend.RequestModels;
using CollectBackend.ResponseModels;
using Microsoft.EntityFrameworkCore;
using Attribute = CollectBackend.Data.Models.Attribute;

namespace CollectBackend.Services
{
    public class EntryService:IEntryService
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public EntryService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public ICollection<EntryResponseModel> GetAllEntries()
        {
            var entries = _applicationDbContext.Entries;
            if (entries.IsNullOrEmpty()) return null;
            var returnValue = entries.Select(entry => MapEntryToEntryResponseModel(entry)).ToList();
            return returnValue;
        }

        public async Task<EntryResponseModel> GetEntryById(Guid id)
        {
            var entry = await _applicationDbContext.Entries.FindAsync(id);
            if (entry == null) return null;
            var returnValue = MapEntryToEntryResponseModel(entry);
            return returnValue;
        }

        public async Task<ICollection<EntryResponseModel>> GetEntryByCollectionId(Guid id)
        {
            var collection = await _applicationDbContext.Collections.FindAsync(id);
            if (collection == null) return null;
            var entries =  _applicationDbContext.Entries.Where(x => x.Collection.CollectionId == id);
            if (entries.IsNullOrEmpty()) return null;
            var returnValue = entries.Select(entry => MapEntryToEntryResponseModel(entry)).ToList();
            return returnValue;
        }

        public async Task<Guid?> AddNewEntry(EntryCreateRequestModel entry)
        {
            var collection = await _applicationDbContext.Collections.FindAsync(entry.CollectionId);
            if (collection is null) return null;

            var imageData = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["data"].Value;
            var imageType = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["type"].Value;

            if (string.IsNullOrEmpty(imageData) || string.IsNullOrEmpty(imageType)) return null;

            var entryToInsert = new Entry()
            {
                Collection = collection,
                Description = entry.Description,
                ItemName = entry.Name,
                Image = Convert.FromBase64String(imageData),
                ImageType = imageType
            };

            await _applicationDbContext.Entries.AddAsync(entryToInsert);

            try
            {
                await _applicationDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            foreach (var attribute in entry.Attributes)
            {
                var attributeFromDb =
                    await _applicationDbContext.Attributes.FirstOrDefaultAsync(x =>
                        x.Name == attribute.First().Key && x.Collection.CollectionId == entry.CollectionId);
                var entryAttributeToInsert = new EntryAttribute()
                {
                    Entry = entryToInsert,
                    Value = attribute.First().Value
                };
                await _applicationDbContext.EntryAttributes.AddAsync(entryAttributeToInsert);
                await _applicationDbContext.SaveChangesAsync();
                if (attributeFromDb == null)
                {
                    var attributeToInsert = new Attribute
                    {
                        Name = attribute.First().Key,
                        EntryAttributes = new List<EntryAttribute> {entryAttributeToInsert},
                    };
                    await _applicationDbContext.Attributes.AddAsync(attributeToInsert);
                }
                else
                {
                    if(attributeFromDb.EntryAttributes.IsNullOrEmpty()) attributeFromDb.EntryAttributes = new List<EntryAttribute>();
                    attributeFromDb.EntryAttributes.Add(entryAttributeToInsert);
                }
            }
            await _applicationDbContext.SaveChangesAsync();

            return entryToInsert.EntryId;
        }

        public async Task<Guid?> AddNewCustomEntry(EntryCustomCreateRequestModel entry)
        {
            var collection = await _applicationDbContext.Collections.FindAsync(entry.CollectionId);
            if (collection is null) return null;

            var imageData = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["data"].Value;
            var imageType = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["type"].Value;

            if (string.IsNullOrEmpty(imageData) || string.IsNullOrEmpty(imageType)) return null;

            var entryToInsert = new Entry()
            {
                Collection = collection,
                Description = entry.Description,
                ItemName = entry.Name,
                Image = Convert.FromBase64String(imageData),
                ImageType = imageType
            };

            await _applicationDbContext.Entries.AddAsync(entryToInsert);

            try
            {
                await _applicationDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            foreach (var attribute in entry.Attributes)
            {
               
                var entryAttributeToInsert = new EntryAttribute()
                {
                    Entry = entryToInsert,
                    Value = attribute.First().Value
                };
                await _applicationDbContext.EntryAttributes.AddAsync(entryAttributeToInsert);
                await _applicationDbContext.SaveChangesAsync();
                var attributeToInsert = new Attribute
                {
                    Name = attribute.First().Key,
                    EntryAttributes = new List<EntryAttribute> { entryAttributeToInsert },
                };
                await _applicationDbContext.Attributes.AddAsync(attributeToInsert);

            }
            await _applicationDbContext.SaveChangesAsync();

            return entryToInsert.EntryId;
        }

        public async Task<bool> RemoveEntry(Guid id)
        {
            var entryToRemove = await _applicationDbContext.Entries.FindAsync(id);
            if (entryToRemove == null) return false;
            var attributesToRemove =
                entryToRemove.EntryAttributes.Select(x => x.Attribute).Where(x => x.Collection == null);
            _applicationDbContext.Attributes.RemoveRange(attributesToRemove);
            await _applicationDbContext.SaveChangesAsync();
            _applicationDbContext.RemoveRange(entryToRemove.EntryAttributes);
            await _applicationDbContext.SaveChangesAsync();
            _applicationDbContext.Entries.Remove(entryToRemove);

            try
            {
                await _applicationDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateEntry(EntryUpdateRequestModel entry)
        {
            var entryFromDb = await _applicationDbContext.Entries.FindAsync(entry.EntryId);
            if (entryFromDb == null) return false;
            var collectionFromDb = await _applicationDbContext.Collections.FindAsync(entry.CollectionId);
            if (collectionFromDb == null) return false;

            var imageData = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["data"].Value;
            var imageType = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["type"].Value;

            entryFromDb.Description = entry.Description;
            entryFromDb.ItemName = entry.Name;
            entryFromDb.Image = Convert.FromBase64String(imageData);
            entryFromDb.ImageType = imageType;

            if (entryFromDb.Collection.CollectionId != entry.CollectionId)
            {
                entryFromDb.Collection = collectionFromDb;
                _applicationDbContext.Entries.Update(entryFromDb);
            }

            var currentAttributesToRemove = entryFromDb.EntryAttributes.Where(x => x.Attribute.Collection == null)
                .Select(x => x.Attribute);
            var currentEntryAttributesToRemove = entryFromDb.EntryAttributes;

            _applicationDbContext.EntryAttributes.RemoveRange(currentEntryAttributesToRemove);
            _applicationDbContext.Attributes.RemoveRange(currentAttributesToRemove);

            foreach (var attribute in entry.Attributes)
            {
                var attributeKey = attribute.First().Key;
                var attributeValue = attribute.First().Value;
                var attributeFromDb = collectionFromDb.Attributes.FirstOrDefault(x => x.Name == attributeKey);
                if (attributeFromDb == null)
                {
                    attributeFromDb = new Attribute() { Collection = collectionFromDb, Name = attributeKey };
                    await _applicationDbContext.Attributes.AddAsync(attributeFromDb);
                }
                var newEntryAttribute = new EntryAttribute() { Attribute = attributeFromDb, Value = attributeValue, Entry = entryFromDb };
                await _applicationDbContext.EntryAttributes.AddAsync(newEntryAttribute);
            }

            await _applicationDbContext.SaveChangesAsync();

            return true;
        }

        // public async Task<bool> UpdateEntry(EntryUpdateRequestModel entry)
        // {
        //     var entryFromDb = await _applicationDbContext.Entries.FindAsync(entry.EntryId);
        //     if (entryFromDb == null) return false;
        //     var imageData = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["data"].Value;
        //     var imageType = Regex.Match(entry.Image, @"data:image/(?<type>.+?);base64,(?<data>.+)").Groups["type"].Value;
        //
        //     var entryAttributesToRemove =
        //         _applicationDbContext.EntryAttributes.Where(x => x.Entry.EntryId == entry.EntryId).ToArray();
        //     var attributesToRemove = _applicationDbContext.EntryAttributes.Where(x => x.Attribute.Collection == null && x.Entry.EntryId  == entry.EntryId)
        //         .Select(x => x.Attribute).ToArray();
        //
        //     _applicationDbContext.EntryAttributes.RemoveRange(entryAttributesToRemove);
        //     await _applicationDbContext.SaveChangesAsync();
        //
        //     _applicationDbContext.Attributes.RemoveRange(attributesToRemove);
        //     await _applicationDbContext.SaveChangesAsync();
        //
        //
        //     if (string.IsNullOrEmpty(imageData) || string.IsNullOrEmpty(imageType)) return false;
        //
        //     entryFromDb.ItemName = entry.Name;
        //     entryFromDb.Description = entry.Description;
        //     entryFromDb.ImageType = imageType;
        //     entryFromDb.Image = Convert.FromBase64String(imageData);
        //
        //     if (entryFromDb.Collection.CollectionId != entry.CollectionId)
        //     {
        //         var collection = await _applicationDbContext.Collections.FindAsync(entry.CollectionId);
        //         if (collection == null) return false;
        //         entryFromDb.Collection = collection;
        //     }
        //
        //
        //
        //     _applicationDbContext.Entries.Update(entryFromDb);
        //     await _applicationDbContext.SaveChangesAsync();
        //
        //
        //
        //     foreach (var attribute in entry.Attributes)
        //     {
        //         var attributeFromDb =
        //             await _applicationDbContext.Attributes.FirstOrDefaultAsync(x =>
        //                 x.Name == attribute.First().Key && x.Collection.CollectionId == entry.CollectionId);
        //         var entryAttributeToInsert = new EntryAttribute()
        //         {
        //             Entry = entryFromDb,
        //             Value = attribute.First().Value
        //         };
        //         await _applicationDbContext.EntryAttributes.AddAsync(entryAttributeToInsert);
        //         await _applicationDbContext.SaveChangesAsync();
        //         if (attributeFromDb == null)
        //         {
        //             var attributeToInsert = new Attribute
        //             {
        //                 Name = attribute.First().Key,
        //                 EntryAttributes = new List<EntryAttribute> { entryAttributeToInsert },
        //             };
        //             await _applicationDbContext.Attributes.AddAsync(attributeToInsert);
        //         }
        //         else
        //         {
        //             if (attributeFromDb.EntryAttributes.IsNullOrEmpty()) attributeFromDb.EntryAttributes = new List<EntryAttribute>();
        //             attributeFromDb.EntryAttributes.Add(entryAttributeToInsert);
        //         }
        //     }
        //
        //     await _applicationDbContext.SaveChangesAsync();
        //
        //     return true;
        // }

        private static EntryResponseModel MapEntryToEntryResponseModel(Entry entry)
        {
            var entryReturn = new EntryResponseModel()
            {
                CollectionId = entry.Collection.CollectionId,
                Description = entry.Description,
                EntryId = entry.EntryId,
                Name = entry.ItemName,
                Image = "data:image/"+entry.ImageType+";base64,"+Convert.ToBase64String(entry.Image),
                Attributes = new List<IDictionary<string, string>>()
            };

            foreach (var entryAttribute in entry.EntryAttributes)
            {
                var key = entryAttribute.Attribute.Name;
                var value = entryAttribute.Value;
                entryReturn.Attributes.Add(new Dictionary<string, string>() {{key, value}});
            }

            return entryReturn;
        }
    }
}