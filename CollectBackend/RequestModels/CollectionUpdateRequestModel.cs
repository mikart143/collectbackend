﻿using System;
using System.Collections.Generic;

namespace CollectBackend.RequestModels
{
    public class CollectionUpdateRequestModel
    {
        public Guid CollectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<string> Attributes { get; set; }
    }
}