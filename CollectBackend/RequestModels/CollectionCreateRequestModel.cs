﻿using System.Collections.Generic;

namespace CollectBackend.RequestModels
{
    public class CollectionCreateRequestModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<string> Attributes { get; set; }

    }
}