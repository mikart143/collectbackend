﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectBackend.Data.Models
{
    public class Entry
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EntryId { get; set; }
        public string ItemName { get; set; }
        public byte[] Image { get; set; }
        public string ImageType { get; set; }
        public string Description { get; set; }
        public virtual Collection Collection { get; set; }
        public virtual ICollection<EntryAttribute> EntryAttributes { get; set; }
    }
}