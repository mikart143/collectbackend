﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectBackend.Data.Models
{
    public class EntryAttribute
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EntryAttributeId { get; set; }
        public string Value { get; set; }
        public virtual Entry Entry { get; set; }
        public virtual Attribute Attribute { get; set; }
    }
}