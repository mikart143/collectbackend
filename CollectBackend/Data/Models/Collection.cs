﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectBackend.Data.Models
{
    public class Collection
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CollectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Attribute> Attributes { get; set; }
        public virtual ICollection<Entry> Entries { get; set; }
    }
}