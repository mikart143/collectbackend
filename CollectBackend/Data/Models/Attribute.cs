﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectBackend.Data.Models
{
    public class Attribute
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AttributeId { get; set; }
        public string Name { get; set; }
        public virtual Collection Collection { get; set; }
        public virtual ICollection<EntryAttribute> EntryAttributes { get; set; }
    }
}