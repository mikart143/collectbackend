﻿using CollectBackend.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace CollectBackend.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options){}
        public DbSet<Collection> Collections { get; set; }
        public DbSet<EntryAttribute> EntryAttributes { get; set; }
        public DbSet<Entry> Entries { get; set; }
        public DbSet<Attribute> Attributes { get; set; }
    }
}