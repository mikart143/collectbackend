﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CollectBackend.Data.Migrations
{
    public partial class AddedAttributeToCollection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CollectionId",
                table: "EntryAttributes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EntryAttributes_CollectionId",
                table: "EntryAttributes",
                column: "CollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_EntryAttributes_Collections_CollectionId",
                table: "EntryAttributes",
                column: "CollectionId",
                principalTable: "Collections",
                principalColumn: "CollectionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EntryAttributes_Collections_CollectionId",
                table: "EntryAttributes");

            migrationBuilder.DropIndex(
                name: "IX_EntryAttributes_CollectionId",
                table: "EntryAttributes");

            migrationBuilder.DropColumn(
                name: "CollectionId",
                table: "EntryAttributes");
        }
    }
}
