﻿using System;
using System.Collections.Generic;

namespace CollectBackend.ResponseModels
{
    public class CollectionResponseModel
    {
        public Guid CollectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public  ICollection<string> Attributes { get; set; }
    }
}