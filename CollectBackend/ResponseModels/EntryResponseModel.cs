﻿using System;
using System.Collections.Generic;

namespace CollectBackend.ResponseModels
{
    public class EntryResponseModel
    {
        public Guid EntryId { get; set; }
        public Guid CollectionId { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<IDictionary<string, string>> Attributes { get; set; }
    }
}