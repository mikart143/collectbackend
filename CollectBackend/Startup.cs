using System.IO;
using CollectBackend.Data;
using CollectBackend.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CollectBackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOpenApiDocument();
            services.AddDbContext<ApplicationDbContext>(opt => opt.UseLazyLoadingProxies().UseSqlite("Data Source=collect_backend.db"));
            services.AddScoped<ICollectionService, CollectionService>();
            services.AddScoped<IEntryService, EntryService>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            using var serviceScope = app.ApplicationServices.CreateScope();
            var ctx = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
            if (!ctx.Database.EnsureCreated() && !File.Exists("collect_backend.db"))
                ctx.Database.Migrate();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(builder => builder
                .AllowAnyMethod()
                .AllowAnyOrigin()
                // .AllowCredentials()
                .AllowAnyHeader());


            app.UseAuthorization();

            app.UseOpenApi(); // serve OpenAPI/Swagger documents
            app.UseSwaggerUi3(); // serve Swagger UI
            app.UseReDoc( opt => opt.Path = "/redoc"); // serve ReDoc UI

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
