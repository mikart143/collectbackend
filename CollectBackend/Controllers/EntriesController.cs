﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CollectBackend.RequestModels;
using CollectBackend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CollectBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class EntriesController : ControllerBase
    {
        private readonly IEntryService _entryService;

        public EntriesController(IEntryService entryService)
        {
            _entryService = entryService;
        }
        // GET: api/<EntriesController>
        [HttpGet]
        public IActionResult Get()
        {
            var response = _entryService.GetAllEntries();
            if (response == null) return NoContent();
            return Ok(response);
        }

        // GET api/<EntriesController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _entryService.GetEntryById(id);

            return response == null ? (IActionResult) NotFound() : Ok(response);
        }

        //GET api/<EntriesController>/asdasd-d-asd
        [HttpGet("Collection/{id}")]
        public async Task<IActionResult> GetByCollection(Guid id)
        {
            var response = await _entryService.GetEntryByCollectionId(id);

            return response == null ? (IActionResult)NotFound() : Ok(response);
        }

        // POST api/<EntriesController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EntryCreateRequestModel value)
        {
            var entryId = await _entryService.AddNewEntry(value);
            return entryId == null ? StatusCode(500) : (IActionResult)CreatedAtAction(nameof(Get), new { id = entryId }, value);
        }
        
        // POST api/<EntriesController>/custom
        [HttpPost("custom")]
        public async Task<IActionResult>  Post([FromBody] EntryCustomCreateRequestModel value)
        {
            var entryId = await _entryService.AddNewCustomEntry(value);
            return entryId == null ? StatusCode(500) : (IActionResult)CreatedAtAction(nameof(Get), new { id = entryId }, value);
        }

        // PUT api/<EntriesController>/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] EntryUpdateRequestModel value)
        {
            var updated = await _entryService.UpdateEntry(value);
            return updated ? Ok(): StatusCode(500);
        }

        // DELETE api/<EntriesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var removed = await _entryService.RemoveEntry(id);
            return removed ? Ok(): StatusCode(500);
        }
    }
}
