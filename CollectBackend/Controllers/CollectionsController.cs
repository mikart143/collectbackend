﻿using System;
using System.Threading.Tasks;
using Castle.Core.Internal;
using CollectBackend.RequestModels;
using CollectBackend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CollectBackend.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : ControllerBase
    {
        private readonly ICollectionService _collectionService;
        public CollectionsController(ICollectionService collectionService)
        {
            _collectionService = collectionService;
        }
        // GET: api/<CollectionsController>
        [HttpGet]
        public IActionResult Get()
        {
            var response = _collectionService.GetAllCollections();
            if (response.IsNullOrEmpty()) return NoContent();
            return Ok(response);
        }

        // GET api/<CollectionsController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _collectionService.GetCollectionById(id);
            if (response == null) return NoContent();
            return Ok(response);
        }

        // POST api/<CollectionsController>
        [HttpPost]
        public IActionResult Post([FromBody] CollectionCreateRequestModel newCollectionCreate)
        {
            var newCollectionId = _collectionService.AddNewCollection(newCollectionCreate);
            return CreatedAtAction(nameof(Get), new {id = newCollectionId}, newCollectionCreate);
        }

        // PUT api/<CollectionsController>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] CollectionUpdateRequestModel collection)
        {
            var updated = await _collectionService.UpdateCollection(collection);
            return updated ? Ok() : StatusCode(500);
        }

        // DELETE api/<CollectionsController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var removed = await _collectionService.RemoveCollection(id);
            return removed ? Ok() : StatusCode(500);
        }
    }
}
